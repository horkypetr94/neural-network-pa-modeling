from data_preprocessing import *
import numpy as np
import matplotlib.pyplot as plt
from numpy import hstack
from keras.preprocessing.sequence import TimeseriesGenerator
from keras.models import Sequential
from keras.layers import Dense, LSTM
from sklearn import preprocessing
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# param 1 [0:1], param 2 [0:50]
# inp, outp = input_dataset(0,20,1)
inp, outp = input_multiple_sets(n_set=0, n_sig=0, multisig=40, split=8)
# inp, outp = sqrt()

in_seq1 = (inp.real)
in_seq2 = (inp.imag)
in_seq3 = abs(inp)

out_seq1 = (outp.real)
out_seq2 = (outp.imag)

# in_stack1 = hstack((in_seq1[:-2], in_seq1[1:-1], in_seq1[2:], in_seq3[:-2]))
# in_stack2 = hstack((in_seq2[:-2], in_seq2[1:-1], in_seq2[2:], in_seq3[:-2]))

in_stack1 = hstack((in_seq1[:-1], in_seq1[1:], in_seq3[:-1]))
in_stack2 = hstack((in_seq2[:-1], in_seq1[1:], in_seq3[:-1]))
#
# in_stack1 = hstack((in_seq1[:], in_seq3[:]))
# in_stack2 = hstack((in_seq2[:], in_seq3[:]))

scaler1 = preprocessing.MinMaxScaler().fit(out_seq1)
out_scaled1 = scaler1.transform(out_seq1)

scaler2 = preprocessing.MinMaxScaler().fit(out_seq2)
out_scaled2 = scaler2.transform(out_seq2)

batchSize = 1000
epochs = 30
in_length = 1
n_input = len(in_stack1[0])

generator = TimeseriesGenerator(in_stack1[1:], out_scaled1[:1-n_input], length=in_length, batch_size=batchSize, reverse=True)
# define model
model = Sequential()
model.add(LSTM(50, return_sequences=True, input_dim=n_input, activation='relu'))
model.add(LSTM(50, return_sequences=True, input_dim=n_input, activation='relu'))
model.add(LSTM(50, return_sequences=True, input_dim=n_input, activation='relu'))
model.add(Dense(32, activation='relu'))
model.add(Dense(1))
model.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])

# fit model
model.fit(generator, epochs=epochs, verbose=1, batch_size=batchSize)

generator2 = TimeseriesGenerator(in_stack2[1:], out_scaled2[:1-n_input], length=in_length, batch_size=batchSize, reverse=True)
# define model
model2 = Sequential()
model2.add(LSTM(50, return_sequences=True, input_dim=n_input, activation='relu'))
model2.add(LSTM(50, return_sequences=True, input_dim=n_input, activation='relu'))
model2.add(LSTM(50, return_sequences=True, input_dim=n_input, activation='relu'))
model2.add(Dense(32, activation='relu'))
model2.add(Dense(1))
model2.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])


# fit model
model2.fit(generator2, epochs=epochs, verbose=1, batch_size=batchSize)

# yhat = model.predict(in_stack1[0:136319], verbose=1)
# yhat2 = model2.predict(in_stack2[0:136320], verbose=1)
corr1 = in_stack1[0:136319]
corr2 = in_stack2[0:136319]

rnum = len(corr1)
yhat = model.predict(corr1.reshape((rnum,1,n_input)), verbose=1)
yhat2 = model2.predict(corr2.reshape((rnum,1,n_input)), verbose=1)

# rescaling
i_yhat = scaler1.inverse_transform(yhat.reshape((rnum,1)))
i_yhat2 = scaler2.inverse_transform(yhat2.reshape((rnum,1)))


yhat_cmplx=i_yhat[:]+1j*i_yhat2[:]

yhat_cmplx=yhat_cmplx.reshape((len(yhat_cmplx), 1))

plt.plot(np.abs(inp[0:rnum]), np.abs(outp[0:rnum]),'.',markersize=2)
plt.plot(np.abs(inp[0:rnum]), np.abs(yhat_cmplx),'.', markersize=2)
plt.show()

def nmse(y,y2):
  return (10*np.log10(np.sum(np.square(np.abs(y-y2)))/np.sum(np.square(np.abs(y)))))

print(nmse(outp[:rnum],yhat_cmplx))
