from keras.preprocessing.sequence import TimeseriesGenerator
from keras.models import Sequential
from tensorflow.keras.layers import LSTM
from keras.layers import Dense, Dropout
from data_preprocessing import *
import numpy as np
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'


def nmse(y,y2):
  return (10*np.log10(np.sum(np.square(np.abs(y-y2)))/np.sum(np.square(np.abs(y)))))

inp, outp = input_dataset(0,5)
epochs = 3
n_input = 2

in_seq1 = (inp.real)
in_seq2 = (inp.imag)
dataset = np.hstack((in_seq1,in_seq2))

out_seq1 = (outp.real)
out_seq2 = (outp.imag)



generator = TimeseriesGenerator(dataset[1:], out_seq1[:-1], length=n_input, batch_size=1)

# define model
model = Sequential()
model.add(LSTM(32, return_sequences = True, input_dim=2, activation='relu'))
model.add(Dropout(0.6))
model.add(LSTM(64, return_sequences = True, input_dim=2, activation='relu'))
model.add(Dropout(0.4))
model.add(LSTM(8, activation='relu'))
model.add(Dense(2))
model.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])


# fit model
model.fit(generator, epochs=epochs, verbose=1, batch_size=40)


yhat = model.predict(generator, verbose=1)
yhat_cmplx=yhat[:,0]+1j*yhat[:,1]

yhat_cmplx=yhat_cmplx.reshape((len(yhat_cmplx), 1))

plt.plot(np.abs(inp), np.abs(outp),'.')
plt.plot(np.abs(inp[:len(yhat_cmplx)]), np.abs(yhat_cmplx),'.')
plt.show()

print(nmse(inp[:len(yhat_cmplx)], yhat_cmplx))

