from numpy import hstack
from keras.preprocessing.sequence import TimeseriesGenerator
from keras.models import Sequential
from keras.layers import Dense
from data_generator import *
import os
from data_preprocessing import *
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'



def nmse(y,y2):
  return (10*np.log10(np.sum(np.square(np.abs(y-y2)))/np.sum(np.square(np.abs(y)))))

# param 1 [0:1], param 2 [0:50]
# v, y = input_dataset(0,20)
v, y = sqrt()

# define dataset
in_seq1 = (v.real)
in_seq2 = (v.imag)

# reshape series
in_seq1 = in_seq1.reshape((len(in_seq1), 1))
in_seq2 = in_seq2.reshape((len(in_seq2), 1))
# horizontally stack columns
dataset = hstack((in_seq1, in_seq2))

out_seq1 = (y.real)
out_seq2 = (y.imag)
#print(out_seq1)
out_seq1 = out_seq1.reshape((len(out_seq1), 1))
out_seq2 = out_seq2.reshape((len(out_seq2), 1))

dataset_out = hstack((out_seq1, out_seq2))

# define generator
n_input = 1
generator = TimeseriesGenerator(dataset[1:], dataset_out[:-1], length=n_input, batch_size=1)

# define model
model = Sequential()
model.add(Dense(32, activation='relu', input_shape=(n_input,2)))
model.add(Dense(16, activation='relu'))
model.add(Dense(2))
model.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])

# fit model
model.fit(generator, epochs=50, verbose=1, batch_size=100)

yhat = model.predict(generator, verbose=2)
yhat_cmplx=yhat[:,0,0]+1j*yhat[:,0,1]

yhat_cmplx=yhat_cmplx.reshape((len(yhat_cmplx), 1))

plt.plot(np.abs(v), np.abs(y),'.')
plt.plot(np.abs(v[0:len(yhat_cmplx)]), np.abs(yhat_cmplx), '.', markersize=2)
plt.show()

def nmse(y,y2):
  return (10*np.log10(np.sum(np.square(np.abs(y-y2)))/np.sum(np.square(np.abs(y)))))

print(nmse(y[:len(yhat_cmplx)], yhat_cmplx))

