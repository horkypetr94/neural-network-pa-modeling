from data_preprocessing import *
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing.sequence import TimeseriesGenerator
from keras.models import Sequential
from keras.layers import Dense, GRU
from sklearn import preprocessing
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# param 1 [0:1], param 2 [0:50]
# inp, outp = input_dataset(0,20,1)
inp, outp = input_multiple_sets(n_set=0, n_sig=0, multisig=50, split=3)
# inp, outp = sqrt()

in_seq1 = (inp.real)
in_seq2 = (inp.imag)

out_seq1 = (outp.real)
out_seq2 = (outp.imag)

scaler1 = preprocessing.MinMaxScaler().fit(out_seq1)
out_scaled1 = scaler1.transform(out_seq1)

scaler2 = preprocessing.MinMaxScaler().fit(out_seq2)
out_scaled2 = scaler2.transform(out_seq2)

batchSize = 100
epochs = 20
n_input = 2

generator = TimeseriesGenerator(in_seq1[:-1], out_scaled1[:-1], length=n_input, batch_size=batchSize, reverse=True)
# define model
model = Sequential()
model.add(GRU(100))
model.add(Dense(1))
model.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])

# fit model
model.fit(generator, epochs=epochs, verbose=1, batch_size=batchSize)

generator2 = TimeseriesGenerator(in_seq2[:-1], out_scaled2[:-1], length=n_input, batch_size=batchSize, reverse=True)
# define model
model2 = Sequential()
model2.add(GRU(100))
model2.add(Dense(1))
model2.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])


# fit model
model2.fit(generator2, epochs=epochs, verbose=1, batch_size=batchSize)

yhat = model.predict(in_seq1[0:136320], verbose=1)
yhat2 = model2.predict(in_seq2[0:136320], verbose=1)
i_yhat = scaler1.inverse_transform(yhat)
i_yhat2 = scaler2.inverse_transform(yhat2)


yhat_cmplx=i_yhat[:]+1j*i_yhat2[:]

yhat_cmplx=yhat_cmplx.reshape((len(yhat_cmplx), 1))

plt.plot(np.abs(inp[0:136320]), np.abs(outp[0:136320]),'.')
plt.plot(np.abs(inp[0:136320]), np.abs(yhat_cmplx),'.')
plt.show()

def nmse(y,y2):
  return (10*np.log10(np.sum(np.square(np.abs(y-y2)))/np.sum(np.square(np.abs(y)))))

print(nmse(outp[0:136320],yhat_cmplx))