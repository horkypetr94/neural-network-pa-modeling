import numpy as np
import matplotlib.pyplot as plt


def power_2(N = 2000):
    IN = np.random.uniform(-1, 1, [N, 1])+1j*np.random.uniform(-1, 1, [N, 1])
    OUT = 3+np.power(IN, 2)*4
    return IN, OUT


def power_3(N = 2000):
    IN = np.random.uniform(-1, 1, [N, 1]) + 1j * np.random.uniform(-1, 1, [N, 1])
    OUT = np.power(IN, 3)
    return IN, OUT


def sqrt(N = 2000):
    IN = np.random.uniform(-1, 1, [N, 1]) + 1j * np.random.uniform(-1, 1, [N, 1])
    OUT = np.sqrt(IN)
    return IN, OUT


def sine(N = 2000):
    IN = np.random.uniform(-1, 1, [N, 1]) + 1j * np.random.uniform(-1, 1, [N, 1])
    OUT = np.sin(np.linspace(0, 2 * np.pi, N))
    return IN, OUT


def mishung(N=2000):
    IN = np.random.uniform(-1, 1, [N, 1]) + 1j * np.random.uniform(-1, 1, [N, 1])
    OUT = np.array([])

    for i in range(len(IN)):
        r = np.random.choice([-0.1, -0.5, 0.1, 0.5, 1])
        newVal = IN[i] + r
        OUT = np.append(OUT, newVal)

    return IN, OUT




