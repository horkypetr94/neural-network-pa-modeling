from tensorflow import keras
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from keras.preprocessing.sequence import TimeseriesGenerator
from data_preprocessing import *
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'



# Load data from MATLAB file and separate I and Q

def load_and_edit():
    data_in, data_out = input_multiple_sets(n_set=0, n_sig=0, multisig=1, split=1)
    inp = abs(data_in)
    outp = np.hstack((data_out.real, data_out.imag))
    teta = np.angle(data_in)

    return data_in, data_out, inp, outp, np.float32(teta)

def teta_to_tensors(teta):
    sin = tf.math.sin(teta)
    cos = tf.math.cos(teta)

    return sin, cos

class Prb(keras.layers.Layer):
    def __init__(self, t_sin, t_cos):
        super(Prb, self).__init__()
        self.t_sin = t_sin
        self.t_cos = t_cos

    def call(self, dense1):
        prb_sin = tf.multiply(self.t_sin, dense1)
        prb_cos = tf.multiply(self.t_cos, dense1)

        return tf.concat([prb_sin, prb_cos], 2)



#########  M O D E L  ###########
def make_model(t_sin,t_cos, input_shape):
    input_layer = keras.layers.Input(input_shape)

    dense1 = keras.layers.Dense(32, activation="relu")(input_layer)
    prb = Prb(t_sin, t_cos)(dense1)
    output_layer = keras.layers.Dense(2, activation="relu")(prb)

    return keras.models.Model(inputs=input_layer, outputs=output_layer)


##############RUN CODE
batch_size = 1
epochs = 1
n_input = 1
in_length = 1

data_in, data_out, inseq, outseq, teta = load_and_edit()
t_sin, t_cos = teta_to_tensors(teta)
generator = TimeseriesGenerator(inseq[1:], outseq[:-1], length=in_length, batch_size=batch_size, reverse=True)

model = make_model(t_sin, t_cos, input_shape=(136318,1))
model.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])
model.fit(generator, epochs=epochs, verbose=1, batch_size=batch_size)
print(counter)

corr = inseq[:136318].reshape((1,136318,1))

yhat = model.predict(corr, verbose=1)

yhat_cmplx=yhat[:,:,0]+1j*yhat[:,:,1]
yhat_cmplx=yhat_cmplx.reshape((len(yhat_cmplx[0]), 1))

plt.plot(np.abs(data_in[:136318]), np.abs(data_out[:136318]),'.')
plt.plot(np.abs(data_in[:136318]), np.abs(yhat_cmplx),'.')
plt.show()

def nmse(y,y2):
  return (10*np.log10(np.sum(np.square(np.abs(y-y2)))/np.sum(np.square(np.abs(y)))))

print(nmse(inseq[:len(yhat_cmplx)], yhat_cmplx))



# for i in range(len(generator)):
#     x, z = generator[i]
#     print('%s => %s' % (x, z))
#     break