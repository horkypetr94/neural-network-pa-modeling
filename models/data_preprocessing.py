import scipy.io
import numpy as np
import matplotlib.pyplot as plt

def input_dataset(n_set = 1, n_sig = 10, split = 2):
    # mat má x,y,z  x=IN, y=OUT, z=je test
    mat = scipy.io.loadmat('rfsoc_nn_training.mat')
    #npx je vstup a má rozmer (2,50,136320)
    #np.array_equal(npx[1,0],npx[0,0]) = TRUE
    npx = np.array(mat['x'])

    # np.array_equal(npy[1,0],npy[0,0]) = FALSE
    npy = np.array(mat['y'])

    # vyberu si kolikaty signal chci pouzit number of set//minule tu bylo abs()
    ax = npx[n_set]
    ay = npy[n_set]

    x = ax[n_sig]
    y = ay[n_sig]

    rx = np.reshape(x, (len(x), 1))
    ry = np.reshape(y, (len(y), 1))

    rxs = rx[::split]
    rys = ry[::split]

    return rxs, rys

def input_multiple_sets(n_set = 1, n_sig = 10, multisig = 2,  split = 2):
    # mat má x,y,z  x=IN, y=OUT, z=je test
    mat = scipy.io.loadmat('rfsoc_nn_training.mat')
    #npx je vstup a má rozmer (2,50,136320)
    #np.array_equal(npx[1,0],npx[0,0]) = TRUE
    npx = np.array(mat['x'])
    # np.array_equal(npy[1,0],npy[0,0]) = FALSE
    npy = np.array(mat['y'])

    # vyberu si kolikaty signal chci pouzit number of set//minule tu bylo abs()
    ax = npx[n_set]
    ay = npy[n_set]

    x = ax[n_sig:n_sig+multisig]
    y = ay[n_sig:n_sig+multisig]

    rx = np.reshape(x, (136320*multisig, 1))
    ry = np.reshape(y, (136320*multisig, 1))

    rxs = rx[::split]
    rys = ry[::split]

    return rxs[1:-1], rys[1:-1]