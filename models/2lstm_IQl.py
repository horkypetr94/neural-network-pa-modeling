from data_preprocessing import *
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing.sequence import TimeseriesGenerator
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
# param 1 [0:1], param 2 [0:50]
inp, outp = input_dataset(0,5)
# inp, outp = sqrt()
epochs = 1
n_input = 1

in_seq1 = (inp.real)
in_seq2 = (inp.imag)
dataset = np.hstack((in_seq1,in_seq2))

out_seq1 = (outp.real)
out_seq2 = (outp.imag)


generator = TimeseriesGenerator(dataset[1:], out_seq1[:-1], length=n_input, batch_size=1)
generator2 = TimeseriesGenerator(dataset[1:], out_seq2[:-1], length=n_input, batch_size=1)


# define MODEL I
model = Sequential()
model.add(LSTM(8, return_sequences = True, input_dim=2, activation='relu'))
model.add(Dropout(0.6))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.6))
model.add(Dense(32, activation='relu'))
model.add(Dense(1))
model.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])

# fit model
model.fit(generator, epochs=epochs, verbose=1, batch_size=100)

# define MODEL Q
model2 = Sequential()
model.add(LSTM(8, return_sequences = True, input_dim=2, activation='relu'))
model.add(Dropout(0.6))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.6))
model.add(Dense(32, activation='relu'))
model.add(Dense(1))
model2.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])

# fit model
model2.fit(generator2, epochs=epochs, verbose=1, batch_size=100)


#pro predict je treba jedna hodnota
n_input = 1
generator = TimeseriesGenerator(dataset[1:], out_seq1[:-1], length=n_input, batch_size=1)
generator2 = TimeseriesGenerator(dataset[1:], out_seq2[:-1], length=n_input, batch_size=1)


yhat = model.predict(generator, verbose=1)
yhat2 = model2.predict(generator2, verbose=1)

yhat_cmplx=yhat[:]+1j*yhat2[:136318]

yhat_cmplx=yhat_cmplx.reshape((len(yhat_cmplx), 1))

plt.plot(np.abs(inp), np.abs(outp),'.')
plt.plot(np.abs(inp[:len(yhat_cmplx)]), np.abs(yhat_cmplx),'.')
plt.show()

def nmse(y,y2):
  return (10*np.log10(np.sum(np.square(np.abs(y-y2)))/np.sum(np.square(np.abs(y)))))

print(nmse(outp[:136318],yhat_cmplx))