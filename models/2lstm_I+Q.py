import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing.sequence import TimeseriesGenerator
from keras.models import Sequential
from keras.layers import Dense, LSTM
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
# param 1 [0:1], param 2 [0:50]
# inp, outp = input_dataset(1,10)



in_seq1 = (inp.real)
in_seq2 = (inp.imag)


out_seq1 = (outp.real)
out_seq2 = (outp.imag)

epochs = 2
n_input = 1


generator = TimeseriesGenerator(in_seq1[:-1], out_seq1[:-1], length=n_input, batch_size=1)
generator2 = TimeseriesGenerator(in_seq2[:-1], out_seq2[:-1], length=n_input, batch_size=1)

# define model
model = Sequential()
model.add(LSTM(32, return_sequences = True, input_dim=1, activation='relu'))
model.add(LSTM(16, return_sequences = True, input_dim=1, activation='relu'))
model.add(Dense(32, activation='relu'))
model.add(Dense(1))
model.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])

# fit model
model.fit(generator, epochs=epochs, verbose=1, batch_size=100)

# define model
model2 = Sequential()
model.add(LSTM(32, return_sequences = True, input_dim=1, activation='relu'))
model.add(LSTM(16, return_sequences = True, input_dim=1, activation='relu'))
model.add(Dense(32, activation='relu'))
model.add(Dense(1))
model2.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])

# fit model
model2.fit(generator2, epochs=epochs, verbose=1, batch_size=100)

yhat = model.predict(in_seq1, verbose=1)
yhat2 = model2.predict(in_seq2, verbose=1)

yhat_cmplx=yhat[:]+1j*yhat2[:,0]

yhat_cmplx=yhat_cmplx.reshape((len(yhat_cmplx), 1))

plt.plot(np.abs(inp), np.abs(outp),'.')
plt.plot(np.abs(inp), np.abs(yhat_cmplx),'.')
plt.show()

def nmse(y,y2):
  return (10*np.log10(np.sum(np.square(np.abs(y-y2)))/np.sum(np.square(np.abs(y)))))

print(nmse(outp,yhat_cmplx))