from tensorflow import keras
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow.keras import layers
from keras.preprocessing.sequence import TimeseriesGenerator
from data_preprocessing import *
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'


# Load data from MATLAB file and separate I and Q

def load_and_edit()():
    data_in, data_out = input_multiple_sets(n_set=0, n_sig=0, multisig=10, split=5)
    inp = np.hstack((data_in.real, data_in.imag))
    outp = np.hstack((data_out.real, data_out.imag))
    teta = np.angle(data_in)

    return data_in, data_out, inp, outp, np.float32(teta)


def PRBBlock(teta, x):
    global counter
    # sine and cos of the angle teta
    prbsin = tf.math.sin(teta[counter])
    prbcos = tf.math.cos(teta[counter])

    # Multiplies all the neurons with sin and cos
    prbsin = tf.multiply(prbsin, x)
    prbcos = tf.multiply(prbcos, x)

    # concatenate 1,1,32 and 1,1,32 into 1,1,64
    prbconcat = tf.concat([prbsin, prbcos], 2)
    print("####### P R B   D O N E ########")

    counter += 1
    return prbconcat


#########  M O D E L  ###########

def make_model(teta, input_shape):
    input_layer = keras.layers.Input(input_shape)
    dense1 = layers.Dense(32, activation="relu")(input_layer)
    PRB = PRBBlock(teta, dense1)
    output_layer = layers.Dense(2, activation="relu")(PRB)

    return keras.models.Model(inputs=input_layer, outputs=output_layer)


##############RUN CODE
batchSize = 1
epochs = 10
n_input = 2
in_length = 1
counter = 0

data_in, data_out, inseq, outseq, teta = load_and_edit()()
generator = TimeseriesGenerator(inseq[1:], outseq[:-1], length=in_length, batch_size=batchSize, reverse=True)

model = make_model(teta, input_shape=(136318,2))
model.compile(optimizer='adam', loss='mae', metrics=['mean_squared_error'])
model.fit(generator, epochs=epochs, verbose=1, batch_size=batchSize)

corr = inseq[:136318].reshape((1,136318,2))

yhat = model.predict(corr, verbose=1)

yhat_cmplx=yhat[:,:,0]+1j*yhat[:,:,1]
yhat_cmplx=yhat_cmplx.reshape((len(yhat_cmplx[0]), 1))

plt.plot(np.abs(data_in[:136318]), np.abs(data_out[:136318]),'.')
plt.plot(np.abs(data_in[:136318]), np.abs(yhat_cmplx),'.')
plt.show()

def nmse(y,y2):
  return (10*np.log10(np.sum(np.square(np.abs(y-y2)))/np.sum(np.square(np.abs(y)))))

print(nmse(inseq[:len(yhat_cmplx)], yhat_cmplx))



# for i in range(len(generator)):
#     x, z = generator[i]
#     print('%s => %s' % (x, z))
#     break